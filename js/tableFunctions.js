//entityMap and escapeHtml borrowed from here
//https://github.com/janl/mustache.js/blob/master/mustache.js#L73
var entityMap = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#39;',
    '/': '&#x2F;',
    '`': '&#x60;',
    '=': '&#x3D;'
};

function escapeHtml(string) {
  return String(string).replace(/[&<>"'`=\/]/g, function (s) {
    return entityMap[s];
  });
};

function ordinalNumber(x) {
  var listOfOrdinals = ["0th","1st","2nd","3rd","4th","5th","6th","7th","8th","9th","10th","11th","12th","13th","14th","15th","16th","17th","18th","19th","20th"];
    return listOfOrdinals[x];
};
  
function writeHeaderCell(content, size = [1,1], classes = null, indent = 6) {
  var headerCell = ' '.repeat(indent) + '<th';

  if (classes != null) {
    headerCell += ' class="' + classes.join(' ') + '"';
  };
  
  if (size[0] != 1) {
    headerCell += ' colspan="' + size[0] + '" scope="colgroup"';
  } else {
    headerCell += ' scope="col"';
  };
  
  if (size[1] != 1) {
    headerCell += ' rowspan="' + size[1] + '"';
  };
  
  headerCell += '>' + content + '</th>\n';
  return headerCell;
};

function writeDataCell(content, style = null, indent = 6) {
  var dataCell = ' '.repeat(indent) + '<td';
  
  if (style != null) {
    dataCell += ' style="' + style + '"';
  };
  
  dataCell += '>' + content + '</td>\n';
  return dataCell;
};

function closeRow(indent = 4) {
  return ' '.repeat(indent) + '</tr>\n';
};

function openRow(indent = 4) {
  return ' '.repeat(indent) + '<tr>\n';
};

//probably will write footnotes in the future
function closeTable() {
  return '  </table>\n</div>';
};

function writeColgroup(columns, indent = 4, ruleRight = true) {
  var colgroupString = ' '.repeat(indent) + '<colgroup ';
  if (ruleRight == true) {
   colgroupString += 'class="ruleRight" ';
  };
  colgroupString += 'span="' + columns + '"></colgroup>\n';
  return colgroupString;
};

function openTable(divClasses = null, tableClasses = null, indent = 0) {

  var tableOpening = '<div';
  
  if (divClasses != null) {
    tableOpening += ' class="' + divClasses.join(' ') + '"';
  }
  
  tableOpening += '>\n'
                + ' '.repeat(indent + 2) + '<table';

  if (tableClasses != null) {
    tableOpening += ' class="' + tableClasses.join(' ') + '"';
  }
  tableOpening += '>\n';
  return tableOpening; 
};

function writeCaption (content, indent = 4) {
  return ' '.repeat(indent) + '<caption>' + content + '</caption>\n'
}