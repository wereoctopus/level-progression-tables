/* an entry of null (no quote marks) will be written as an en-dash in the output table, for levels where there are no class features. Otherwise, just wrap the entire set of class features for any given level in quote marks.

If you find the table stretching too much, try including <br> tags to force a line break.

If line breaks are happening where you don't want them, write &nbsp; instead of a space.

*/ 


features = [
  null,
  null,
  null,
  null,
  null,
  null,
  null,
  null,
  null,
  null,
  null,
  null,
  null,
  null,
  null,
  null,
  null,
  null,
  null,
  null
]

/*example of a populated array. Remember the default html generators only look for the features array, it doesn't know to look for bardFeatures, clericFeatures, etc */
bardFeatures = [
  'Bardic music, bardic knowledge,<br>countersong, fascinate, inspire courage +1',
  null,
  'Inspire competence',
  null,
  null,
  'Suggestion',
  null,
  'Inspire courage +2',
  'Inspire greatness',
  null,
  null,
  'Song of freedom',
  null,
  'Inspire courage +3',
  'Inspire heroics',
  null,
  null,
  'Mass suggestion',
  null,
  'Inspire courage +4'
]